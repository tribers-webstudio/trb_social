# Tribes Social

A Drupal 8 module suite for Social behaviors.

## Template examples

We provided default templates as example in the `templates` folder. Simply copy/paste them as-is in your own custom theme.

Clear caches... and voila :)
